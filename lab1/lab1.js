let rowToEdit;
let rowToDelete;

const btnNotify = document.querySelector(".btn-msg");
btnNotify.addEventListener("click", toggleNotifyState);

const studentWindow = document.querySelector(".student-window");
const form = document.querySelector(".student-form");
const tableContent = document.querySelector(".table-content");
const studentDeleteWindow = document.querySelector(".delete-student-window");

const btnAddStudentForm = document.querySelector(".btn--form-submit");
btnAddStudentForm.addEventListener("click", addStudent);

const btnAddStudent = document.querySelector(".btn--add-student");
btnAddStudent.addEventListener("click", openAddStudentWindow);

const btnCloseStudentWindow1 = studentWindow.querySelector(".btn--close-student-window");
btnCloseStudentWindow1.addEventListener("click", closeStudentWindow);

const btnCloseStudentWindow2 = studentWindow.querySelector(".btn--form-cancel");
btnCloseStudentWindow2.addEventListener("click", closeStudentWindow);

const btnDeleteStudentSubmit = studentDeleteWindow.querySelector(".btn--delete-student-submit");
btnDeleteStudentSubmit.addEventListener("click", deleteStudentSubmit);

const btnDeleteStudentCancel1 = studentDeleteWindow.querySelector(".btn--delete-student-cancel");
btnDeleteStudentCancel1.addEventListener("click", closeDeleteWindow);

const btnDeleteStudentCancel2 = studentDeleteWindow.querySelector(".btn--close-student-window");
btnDeleteStudentCancel2.addEventListener("click", closeDeleteWindow);

function toggleNotifyState() {
  btnNotify.classList.toggle("new-msg");
}

function openAddStudentWindow() {
  studentWindow.classList.add("opened");
  const studentWindowHeading = studentWindow.querySelector("h3");
  studentWindowHeading.innerHTML = "Add student";
  form.querySelectorAll("input").forEach((input) => (input.value = ""));
  form.querySelectorAll("select").forEach((select) => (select.value = ""));
}

function addStudent(e) {
  let isFormValid = form.checkValidity();
  if (!isFormValid) {
    form.reportValidity();
  } else {
    e.preventDefault();
    form.checkValidity();
    
    let group, name, gender, birthday;

    group = form.querySelector('[name="group"]').value;
    name = form.querySelector('[name="first-name"]').value + " " + form.querySelector('[name="last-name"]').value;
    gender = form.querySelector('[name="gender"]').value;
    birthday = form.querySelector('[name="birthday"]').value;

     if (studentWindow.querySelector("h3").textContent == "Add student") {
      tableContent.insertAdjacentHTML(
        "beforeend",
        `
    <tr>
      <td>
        <input class="checkbox-status" type="checkbox" class=""/>
      </td>
      <td>
        ${group}
      </td>
      <td>
        ${name}
      </td>
      <td>
        ${gender}
      </td>
      <td>
        ${birthday}
      </td>
      <td>
        <div class="status-indicator"></div>
      </td>
      <td>
        <div class="option-buttons">
          <button class="btn--edit">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="option-icon">
            <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
            </svg>
          </button>
          <button class="btn--delete">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="option-icon">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
            </svg>
          </button>
        </div>
      </td>
    </tr>
    `
      );

      const checkboxes = tableContent.querySelectorAll('[type="checkbox"]');
      checkboxes[checkboxes.length - 1].addEventListener("change", ChangeOnlineStatus);

      const editBtns = tableContent.querySelectorAll(".btn--edit");
      editBtns[editBtns.length - 1].addEventListener("click", openEditStudentWindow);

      const deleteBtns = tableContent.querySelectorAll(".btn--delete");
      deleteBtns[deleteBtns.length - 1].addEventListener("click", openDeleteStudentWindow);
      
     } else {
       const tds = rowToEdit.querySelectorAll("td");
       
       tds[1].innerHTML = group;
       tds[2].innerHTML = name;
       tds[3].innerHTML = gender;
       tds[4].innerHTML = birthday;
     }
    studentWindow.classList.remove("opened");
  }
}


function closeStudentWindow() {
  studentWindow.classList.remove("opened");
}

function ChangeOnlineStatus(e) {
  const checkbox = e.target;
  const statusIndicator =
    checkbox.parentElement.parentElement.querySelector(".status-indicator");
  statusIndicator.classList.toggle("active");
}

function openEditStudentWindow(e) {

  studentWindow.classList.add("opened");
  const studentWindowHeading = studentWindow.querySelector("h3");
  studentWindowHeading.innerHTML = "Edit student";

  const currentRow = e.target.parentElement.parentElement.parentElement.parentElement;
  rowToEdit = currentRow;

  const tds = currentRow.querySelectorAll("td");
  const inputs = form.querySelectorAll("input");
  const selects = form.querySelectorAll("select");
  
  inputs[0].value = tds[1].innerText;
  inputs[1].value = tds[2].innerText.split(" ")[0];
  inputs[2].value = tds[2].innerText.split(" ")[1];
  inputs[3].value = tds[4].innerText;
  selects[0].value = tds[3].innerText;
}

function openDeleteStudentWindow(e) {
  studentDeleteWindow.classList.add("opened");
  rowToDelete = e.target.parentElement.parentElement.parentElement.parentElement;
  const name = studentDeleteWindow.querySelector(".student-to-delete");
  const tds = rowToDelete.querySelectorAll("td");
  name.innerText = tds[2].innerText;
}

function closeDeleteWindow(e) {
  studentDeleteWindow.classList.remove("opened");
}

function deleteStudentSubmit(e) {
  e.preventDefault();
  rowToDelete.outerHTML = "";
  studentDeleteWindow.classList.remove("opened");
}